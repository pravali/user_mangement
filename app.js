const express = require('express');
const app = express();
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');


// Middle wares
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({limit: "100mb"}));
app.use(bodyParser.urlencoded({extended: true, limit: "100mb"}));

//Route variables
const user = require('./routes/users');

//Routes
app.use('/', user);

module.exports = app;