#Follow below steps to run application

npm install

node server.js


#Api endpoints

#Add user
http://localhost:5000/users (POST method)

#Get all users list
http://localhost:5000/users (GET method)

#Get user by userId key
http://localhost:5000/users/39908 (GET method)

#Delete user by userId key
http://localhost:5000/users/39908 (DELETE method)