const router = require("express").Router();
const config = require('../config/config');
const { dbConnect } = require('../database/db');
const randomstring = require('randomstring');


//schema variables
const User = require('../models/user');

router.post("/users", async (req, res) => {
    try{
        const bodyParams = req.body;
        const { name, email, dob } = bodyParams

        if(name && email && dob){
            await dbConnect()
            let userId = randomstring.generate({
                length: 5,
                charset: 'numeric'
            }); 
            let params = {
                userId,
                name,
                email,
                dob
            }
            await User.insertMany(params)
            return res.send({status:"success", message:"User added successfully!"})
        }else{
            return res.send({status:"failure", message:"name,email,dob fields required"})
        }
    }catch(error){
        return res.send({status:"failure", message: error.message})
    }
    
})

router.get("/users", async (req, res) => {
    try{
        await dbConnect()
        const users = await User.find({})
        if(users){
            return res.send({status:"success", data:users})
        }
    }catch(error){
        return res.send({status:"failure", message: error.message})
    }
    
})

router.get("/users/:id", async (req, res) => {
    try{
        const bodyParams = req.params;
        const { id } = bodyParams

        if(id){
            await dbConnect()
            const user = await User.findOne({userId:id})
            if(user){
                return res.send({status:"success", data:user})
            }else{
                return res.send({status:"failure", message:"User not found!"})
            }
        }else{
            return res.send({status:"failure", message:"id field required!"})
        }
    }catch(error){
        return res.send({status:"failure", message: error.message})
    }
    
})

router.delete("/users/:id", async (req, res) => {
    try{
        const bodyParams = req.params;
        const { id } = bodyParams

        if(id){
            await dbConnect()
            const user = await User.deleteOne({userId:id})
            if(user){
                if(user.deletedCount == 1){
                    return res.send({status:"success", message:"User deleted successfully!"})
                }else{
                    return res.send({status:"failure", message:"User not found!"})
                }
            }else{
                return res.send({status:"failure", message:"User not found!"})
            }
        }else{
            return res.send({status:"failure", message:"id field required!"})
        }
    }catch(error){
        return res.send({status:"failure", message: error.message})
    }
    
})

module.exports = router