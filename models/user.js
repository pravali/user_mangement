
'use strict;'
const mongoose = require('mongoose');
const tableName = `users_data`;

const userSchema = mongoose.Schema({
    userId: Number,
	name:String,
	email: String,
	dob: String,
});

module.exports = mongoose.model(tableName, userSchema, tableName)